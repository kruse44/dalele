# Dalele

## Inhalt

Diese Repo stellt die Sammlung aller produzierten Inhalte für die Veranstaltung "Daten Lesen Lernen" an der Universität Göttingen dar.

## Ordnerstruktur

### Grossuebung
Der Ordner "Großübung" beeinhält alle Juptyer Notebooks und damit verbundenen Materialen des Semester SoSe 2020

### Alte Materialien
Der Ordner beinhält die Vorlesungsfolien des letzten Durchlaufes. Außerdem die dazu gehörenden Jupyter-Notebooks, Daten und Grafiken.

### Uebungen
Der Ordner beinhält alle Übungen der letzten Jahre inkl. aller Case-Studies welche gerade under review sind, sowie der dazugehörenden Daten und License

### Case Studies
tba 