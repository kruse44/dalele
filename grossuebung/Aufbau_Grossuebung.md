# Dalele -- Großübung

Die Großübung soll in drei Blöcke eingeteilt werden:„Python als Taschenrechner“, „Daten in Python“ und „Data Wrangling“ sein

0. [Einführung in Jupyter-Notebooks](#jupytereinführung)    
1. [Python als Taschenrechner](#pythoncalc)
2. [Daten in Python](#data2python)
3. [Data Wrangling](#datawrangling)

Inhaltich würde ich es wie folgt strukturieren:
____
<a name="jupytereinführung"></a>0. Block: Einführung in Jupyter-Notebooks
===

hier werde ich mich stark an die Materialien aus dem letzten Jahr orientieren. Und einige grundlegende Sachen hinzufügen wie das Help-System

* [Einführung in Jupyter-Notebooks](#einfuehrung)
    * [Warum Jupyter-Notebooks?](#warumnotebooks)
    * [Wie benutzt man Jupyter-Notebooks?](#wienotebooks)
    * [Das Dashboard](#dashboard)
    * [Der modale Editor](#editor)
    * [Zelltypen](#zelltypen)
    * [Markdown in Text-Zellen](#markdown)
    * [Die wichtigsten Kommandos](#kommandos)
    * [Help-system](#Hilfe)

... genauere Ausführungen weggelassen, da mehr oder minder das selbe wie letztes Jahr 
____

<a name="pythoncalc"></a>1. Block: Python als Taschenrechner
===


* [Python als Taschenrechner](#taschenrechner)
  * [Operators](#operators)
  * [Delimiters](#delimiters)
  * ([Booleans/Comparions operators](#boo)) .... velleicht
  * [Logical operators](#logic)
  * [Variablen](#variable)
  * [Calls](#aufruf)


<a name="operators"></a>Operators
---

Operatoren

| Operator | Effekt                | Beispiel      | Operator   | Effekt           | Beispiel |
| -------- | --------------------- | ------------- | ---------- | ---------------- | -------- |
| +        | Addition              | 1 + 2         | `&`        | 
| -        | Substraktion          | 1 - 2         | `|`        |          
| *        | Multiplikation        | 1 * 2         | ^          |
| /        | Division              | 1 / 2         | ==         |
| **       | to the power          | 1 ** 2        | !=         |
| //       | Floor-divide          | 1 // 2        | <, >       | Kleiner/Größer   |          |
| <=, >=   | Kleiner/größer gleich |a <= b, a >= b | is, is not |

<a name="delimiters"></a>Delimiters
---
....

<a name="boo"></a>Booleans/Comparisons
---

<a name="logic"></a>Logical operators
---

<a name="variable"></a>Variablen
---

<a name="aufruf"></a>Calls
---


<a name="data2python"></a>2. Block: Daten in Python
===

* [Data Types](#datentypen)
    * int, float, str, bool, list, tuple, dict, st, None
* [Listen](#list)
  * [Listen erstellen](#erstellen)
  * [Auf Listenelemente zugreifen](#zugreifen)
  * [Auf Bereiche in Listen zugreifen](#bereiche)
  * [Der ````range()````-Befehl](#range)
  * [Append, insert, sort, pop usw.](#append)
* [Tuples](#tuples)
* [Dictionaries](#Dics)
* [Sets](#sets)
    * [set operations]
* [Exkurs: Software Libraries](#libs)
* [Intro 2 Pandas](#pandas)
  * wozu pandas? wofür ist es gut?
  * Series
  * dataframe
  * indexing von Objekten
  * Reindexing
  * selection
  * filtering
  * Missing data
* Einlesen von Daten mit Pandas
  *  .csv, .txt, .xls
* Remote data
  * pandas_datareader
* exportieren von Daten
  * .csv, 

____
<a name="data2python"></a>3. Block: Data Wrangling
===

https://nbviewer.jupyter.org/github/pydata/pydata-book/blob/2nd-edition/ch07.ipynb

Clean, Transform, Merge und Reshape Data

* Combining/Merging
  * pandas.merge
    * Argumente: left, right, how, sort ..
    * Mergen auf Index
  * pandas.concat
  * combine_first
* Missing Data
    * Filtering out: .dropna()
    * Filling in:
* Data Transformation
    * removing duplicates: drop_duplicated()
    * Transform via function or mapping: .map()
    * Replacing values: .replace()
    * rename: .rename()
    * Discretization and Binning: .cut(), value_counts(), .qcut()
    * Themen raus: 
        * (Detecting/Filtering Outlier) 
        * Permutation und Random Sampling
        * Indicator/Dummy Variables
* String Manipulation auch eher raus ... 